<?php

use App\Http\Sockets\Controller ;

/*
 *  Routes for WebSocket
 *
 * Add route (Symfony Routing Component)
 */

$socket->route( '/' , new Controller( ) , [ '*' ] ) ;