<?php

// GET / - index page
Route::get( '/' , 'Controller@index' ) ;

// POST /csrf - get CSRF token
Route::post( '/csrf' , 'Controller@csrf' )->middleware( 'json' ) ;

// POST /send, session_id, user_id_to, amount - send amount to user_id_to
Route::post( '/send' , 'Controller@send' )->middleware( 'json' ) ;

// POST /trans, session_id, csrf - get current user's transactions
Route::post( '/trans' , 'Controller@trans' )->middleware( 'json' ) ;

// POST /user, session_id, csrf - get user data
Route::post( '/user' , 'Controller@user' )->middleware( 'json' ) ;

// POST /users, session_id, csrf - get all users data
Route::post( '/users' , 'Controller@users' )->middleware( 'json' ) ;

// POST /logout, session_id, csrf - logout
Route::post( '/logout' , 'Controller@logout' )->middleware( 'json' ) ;

// POST /login, email, passwd, csrf - login
Route::post( '/login' , 'Controller@login' )->middleware( 'json' ) ;

// POST /register, name, email, passwd, csrf - registration
Route::post( '/register' , 'Controller@register' )->middleware( 'json' ) ;