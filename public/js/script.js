jQuery( function( ) {
	{
		let $csrf = jQuery( "<input>" ).attr( {
			"type" : "hidden" ,
			"name" : "_token"
		} ).val( jQuery( "html" ).data( "csrf" ) ).addClass( "csrf" ) ;

		let $session_id = jQuery( "<input>" ).attr( {
			"type" : "hidden" ,
			"name" : "session_id"
		} ).addClass( "session_id" ) ;

		let $error = jQuery( "<div>" ).addClass( "error both" ) ;

		jQuery( "form" ).each( function( ) {
			jQuery( this ).
				append( $csrf.clone( true ) ).
				append( $session_id.clone( true ) ).
				append( $error.clone( true ) ) ;
		} ) ;
	}

	let $setValues = function( $self , $data ) {
		for ( $key in $data ) {
			$self.find( "*[data-field=" + $key + "]" ).text( $data[ $key ] ) ;
			$self.find( "*[name=" + $key + "]" ).val( $data[ $key ] ) ;
		}
	} ;

	jQuery( ".ajax-update" ).each( function( ) {
		let $self = jQuery( this ) ;
		let $targetRef = $self.data( "target" ) ;
		let $target = $targetRef ? $self.find( $targetRef ) : $self ;
		let $targetTagName = String( $target.prop( "tagName" ) ).toLowerCase( ) ;
		let $lastResult ;

		let $int_id = setInterval( function( ) {
			if ( ! document.hasFocus( ) || ! $self.is( ":visible" ) ) {
				return ;
			}

			$formProcess.call( $self , function( $data ) {
				if ( ! $data.result ) {
					return ;
				}

				let $preResult = JSON.stringify( $data.result ) ;

				if ( $lastResult == $preResult ) {
					return ;
				}

				$lastResult = $preResult ;

				switch ( $targetTagName ) {
					case "datalist" : {
						$target.empty( ) ;

						jQuery( $data.result ).each( function( $i , $item ) {
							let $option = jQuery( "<option>" ).
								val( $item.id ).text( $item.name + " <" + $item.email + ">" ) ;

							$target.append( $option ) ;
						} ) ;

						break ;
					}
					case "tbody" : {
						let $tr = $target.find( "tr:first" ).clone( true ).removeClass( "nod" ) ;

						$target.empty( ) ;

						jQuery( $data.result ).each( function( $i , $item ) {
							let $trCurrent = $tr.clone( true ) ;

							$setValues( $trCurrent , $item ) ;

							$target.append( $trCurrent ) ;
						} ) ;

						break ;
					}
					default : {
						$setValues( $self , $data.result ) ;

						break ;
					}
				}
			} ) ;
		} , $self.data( "timeout" ) ) ;

		$self.attr( "data-interval-id" , $int_id ) ;
	} ) ;

	jQuery( ".form-logout" ).on( "submit" , function( ) {
		location.reload( ) ;

		return false ;
	} ) ;

	let $formProcess = function( $success ) {
		let $self = jQuery( this ) ;

		jQuery.ajax( {
			"url" : $self.attr( "action" ) ,
			"type" : $self.attr( "method" ) ,
			"data" : $self.serialize( ) ,
			"dataType" : "json" ,
			"success" : function( $data , $textState , $xhr ) {
				if ( $xhr.status == 419 ) {
					location.reload( ) ;
				}

				jQuery( "html" ).attr( {
					"data-csrf" : $data.csrf ,
					"data-session_id" : $data.session_id ,
					"data-perms" : $data.perms ,
					"data-banned" : $data.banned
				} ) ;

				if ( $data.user_id ) {
					jQuery( "html" ).attr( {
						"data-user_id" : $data.user_id
					} ) ;
				}

				jQuery( ".csrf" ).val( $data.csrf ) ;
				jQuery( ".session_id" ).val( $data.session_id ) ;

				if ( "error" in $data ) {
					if ( $self.find( ".error" ).text( $data.error ).length ) {
						return ;
					}

					alert( $data.error ) ;
				}

				$self.find( ".error" ).text( "" ) ;

				$success( $data ) ;
			}
		} ) ;

		return false ;
	} ;

	jQuery( ".form-process" ).on( "submit" , function( ) {
		let $self = jQuery( this ) ;

		return $formProcess.call( $self , function( ) { } ) ;
	} ) ;

	jQuery( ".trans-table tbody tr" ).on( "click" , function( ) {
		let $self = jQuery( this ) ;
		let $data = $self.find( ".trans-receiver-sender" ) ;
		let $receiver_id = $data.find( ".trans-receiver_id" ).text( ) ;
		let $sender_id = $data.find( ".trans-sender_id" ).text( ) ;
		let $user_id = jQuery( "html" ).data( "user_id" ) ;
		let $amount = $self.find( ".trans-amount" ).text( ) ;
		let $form = jQuery( ".form-process.form-send" ) ;

		$form.find( "input[name=user_id_to]" ).val( $user_id == $receiver_id ? $sender_id : $receiver_id ) ;
		$form.find( "input[name=amount]" ).val( $amount ) ;
	} ) ;
} ) ;