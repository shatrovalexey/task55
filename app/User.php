<?php

/**
* Модель пользователя
*/

namespace App;

use DB ;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

	protected $table = 'users' ;

	public $rules = [
		'name' =>  'required|string' ,
		'email' => 'required|email|unique'
	] ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name' , 'email' , 'passwd' ,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = [ 'passwd' ] ;

	/**
	* Установить дату\время истечения сессии
	* @param User $user - пользователь
	* @return void
	*/

	protected static function setExpires( $user ) {
		$user->expires_at = DB::raw( 'adddate( now( ) , INTERVAL 10 MINUTE )' ) ;
	}

	/**
	* Список пользователей
	* @return Collection
	*/

	public static function list( ) {
		return self::select( [ 'id' , 'name' , 'email' , 'admin' , 'banned' ] )->get( ) ;
	}

	/**
	* Зарегистрировать пользователя
	* @param string $name - имя
	* @param string $email - e-mail
	* @param string $passwd - пароль
	* @param boolean $admin - если это администратор
	* @return string - строка ошибки
	*/

	public static function register( $name , $email , $passwd , $admin = false ) {
		$user = new self( ) ;

		$user->name = $name ;
		$user->email = $email ;
		$user->passwd = $passwd ;
		$user->admin = $admin ;

		try {
			$user->save( ) ;
		} catch ( \Exception $exception ) {
			return $exception->getMessage( ) ;
		}

		return null ;
	}

	/**
	* Авторизовать пользователя
	* @param string $email - e-mail
	* @param string $passwd - пароль
	* @return User - пользователь
	*/

	public static function login( $email , $passwd ) {
		$user = self::where( 'email' , $email )
			->where( 'passwd' , $passwd )
			->where( 'banned' , '<' , true )->first( ) ;

		if ( empty( $user ) ) {
			return null ;
		}

		self::setExpires( $user ) ;
		$user->session_id = sha1( uniqid( ) ) ;
		$user->save( ) ;

		return $user ;
	}

	/**
	* Разавторизовать пользователя
	* @param string $session_id - идентификатор сессии
	* @return boolean - результат
	*/

	public static function logout( $session_id ) {
		$user = self::getBySessionId( $session_id , false ) ;

		if ( empty( $user ) ) {
			return false ;
		}

		$user->expires_at = $user->session_id = null ;
		$user->save( ) ;

		return true ;
	}

	/**
	* Получить идентификатор пользователя по ID сессии
	* @param string $session_id - идентификатор сессии
	* @param boolean $update - обновить дату истечения сессии
	* @return boolean - результат
	*/

	public static function getIdBySessionId( $session_id , $update = true ) {
		if ( $user = self::getBySessionId( $session_id , $update ) ) {
			return $user->id ;
		}

		return null ;
	}

	/**
	* Получить пользователя по ID сессии
	* @param string $session_id - идентификатор сессии
	* @param boolean $update - обновить дату истечения сессии
	* @return User - результат
	*/

	public static function getBySessionId( $session_id , $update = true ) {
		$user = self::where( 'session_id' , $session_id )
			->where( 'expires_at' , '>' , DB::raw( 'now( )' ) )
			->where( 'banned' , '<' , true )
			->first( ) ;

		if ( empty( $user ) ) {
			return null ;
		}

		if ( empty( $update ) ) {
			return $user ;
		}

		self::setExpires( $user ) ;
		$user->save( ) ;

		return $user ;
	}
}
