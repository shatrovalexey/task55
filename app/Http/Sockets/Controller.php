<?php

namespace App\Http\Sockets;

use Orchid\Socket\BaseSocketListener;
use Ratchet\ConnectionInterface;
use App\Http\Controllers\Controller AS ControllerCTRL ;

class Controller extends BaseSocketListener {
	/**
	* Current clients.
	*
	* @var SplObjectStorage
	* @var PointModel
	*/
	protected $clients ;
	protected $controller ;

	/**
	* Point constructor.
	*/
	public function __construct( ) {
		$this->clients = new \SplObjectStorage( ) ;
		$this->controller = new ControllerCTRL( ) ;
	}

	/**
	* @param ConnectionInterface $conn
	*/
	public function onOpen( ConnectionInterface $conn ) {
		$this->clients->attach( $conn ) ;
	}

    /**
     * @param ConnectionInterface $from
     * @param $msg
     */
    public function onMessage( ConnectionInterface $from , $msg ) {
	$msg = @json_decode( $msg ) ;

	if ( empty( $msg ) || empty( $msg->session_id ) || empty( $msg->action ) ) {
		return ;
	}

        foreach ( $this->clients as $client ) {
		if ( $from != $client ) {
			continue ;
		}

		$result = $this->controller->execute( $msg ) ;

		if ( empty( $result ) ) {
			continue ;
		}

		$result->action = $msg->action ;

		$result = json_encode( $result ) ;
                $client->send( $result ) ;

		break ;
        }
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception          $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }
}
