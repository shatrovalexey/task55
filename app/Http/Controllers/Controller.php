<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Trans as TransModel ;
use App\User as UserModel ;

/**
* Контроллер
*/

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	* Отправить перевод с обозом
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function send( Request $request ) {
		$session_id = $request->input( 'session_id' ) ;
		$user_id_to = $request->input( 'user_id_to' ) ;
		$amount = $request->input( 'amount' ) ;

		if ( $user_id_from = UserModel::getIdBySessionId( $session_id ) ) {
			if ( $user_id_from == $user_id_to ) {
				return [ 'error' => 'Not supported' ] ;
			}

			return [
				'error' => TransModel::send( $user_id_from , $user_id_to , $amount )
			] ;
		}

		return [ ] ;
	}

	/**
	* Список транзакций пользователя
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function trans( Request $request ) {
		$session_id = $request->input( 'session_id' ) ;
		$user = UserModel::getBySessionId( $session_id ) ;

		if ( empty( $user ) ) {
			return [ 'error' => 'User not found' ] ;
		}

		$trans = TransModel::get( $user->id ) ;

		if ( empty( $trans ) ) {
			return [ 'error' => 'No transactions found' , ] ;
		}

		return [ 'result' => $trans ] ;
	}

	/**
	* Список пользователей
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function users( Request $request ) {
		$session_id = $request->input( 'session_id' ) ;
		$result = [ ] ;

		if ( $user = UserModel::getBySessionId( $session_id ) ) {
			$result[ 'result' ] = UserModel::list( ) ;
		}

		return $result ;
	}

	/**
	* Информация о текущем пользователе
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function user( Request $request ) {
		$session_id = $request->input( 'session_id' ) ;

		if ( $user = UserModel::getBySessionId( $session_id ) ) {
			return [
				'result' => [
					'amount' => $user->amount ,
					'name' => $user->name ,
					'email' => $user->email
				] ,
			] ;
		}

		return [ ] ;
	}

	/**
	* Зарегистрировать пользователя
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function register( Request $request ) {
		$name = $request->input( 'name' ) ;
		$email = $request->input( 'email' ) ;
		$passwd = $request->input( 'passwd' ) ;

		if ( UserModel::register( $name , $email , $passwd ) ) {
			return [ 'error' => 'User exists' ] ;
		}

		return self::login( $request ) ;
	}

	/**
	* Зарегистрировать пользователя
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function login( Request $request ) {
		$email = $request->input( 'email' ) ;
		$passwd = $request->input( 'passwd' ) ;

		$user = UserModel::login( $email , $passwd ) ;

		if ( empty( $user ) ) {
			return [ 'error' => 'No such user' ] ;
		}

		return [ 'session_id' => $user->session_id ] ;
	}

	/**
	* Разавторизация пользователя
	* @param Request $request - запрос
	* @return mixed - результат
	*/

	public function logout( Request $request ) {
		$session_id = $request->input( 'session_id' ) ;

		return [ 'success' => UserModel::logout( $session_id ) ] ;
	}

	/**
	* Главная страница
	* @return Response
	*/

	public function index( ) {
		return view( 'home' ) ;
	}
}
