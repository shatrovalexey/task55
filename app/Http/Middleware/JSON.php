<?php

namespace App\Http\Middleware;

use App\User as UserModel ;
use Closure;

/**
* Фильтр для ответов в формате JSON
*/

class JSON
{
    /**
     * Handle an incoming request.
     * Добавляет в вывод csrf, session_id, perms, banned
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	$response = $next( $request ) ;

	$data = $response->getData( ) ;

	if ( ! is_object( $data ) ) {
		return $response ;
	}

	$data->csrf = csrf_token( ) ;

	if ( empty( $data->session_id ) ) {
		$data->session_id = $request->input( 'session_id' ) ;
	}

	if ( $user = UserModel::getBySessionId( $data->session_id ) ) {
		$data->user_id = $user->id ;
		$data->perms = $user->admin ? 'admin' : 'user' ;
		$data->banned = $user->banned ;
	}

	if ( empty( $data->error ) && empty( $data->message ) ) {
		$data->message = 'OK' ;
	}

	$response->setData( $data ) ;

        return $response ;
    }
}
