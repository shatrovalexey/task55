<?php

namespace App;

use DB ;
use Illuminate\Database\Eloquent\Model;
use App\User as UserModel ;

/**
* Модель транзакций
*/

class Trans extends Model
{
	protected $table = 'trans' ;
	protected $limit = 100 ;

	/**
	* Список транзакций
	* @param integer $user_id - идентификатор пользователя
	* @return Collection - результат
	*/

	public static function get( $user_id = null ) {
		$query = self::orderBy( 'created_at' , 'DESC' )->distinct( ) ;

		if ( ! empty( $user_id ) ) {
			$query->where( 'user_id_from' , $user_id )->
				orWhere( 'user_id_to' , $user_id ) ;
		}

		$query->join( 'users AS u1' , 'u1.id' , '=' , 'trans.user_id_from' )
			->join( 'users AS u2' , 'u2.id' , '=' , 'trans.user_id_to' )
			->select( DB::raw( '
`trans`.`id` ,
`trans`.`amount` ,
`trans`.`created_at` ,
`u1`.`name` AS `sender_name` ,
`u2`.`name` AS `receiver_name` ,
`u2`.`id` AS `receiver_id` ,
`u1`.`id` AS `sender_id`
		' ) ) ;

		// die( $query->toSql( ) ) ;

		return $query->get( ) ;
	}

	/**
	* Отправить средства переводом с обозом
	* @param integer $user_id_from - идентификатор отправителя
	* @param integer $user_id_to - идентификатор получателя
	* @return string - строка ошибки
	*/

	public static function send( $user_id_from , $user_id_to , $amount ) {
		if ( $user_id_from == $user_id_to ) {
			return 'Not supported' ;
		}

		DB::beginTransaction( ) ;

		try {
			$user_from = UserModel::where( 'id' , $user_id_from )->
				where( 'banned' , '<' , true )->first( ) ;

			if ( empty( $user_from ) ) {
				throw new \Exception( 'Sender not found' ) ;
			}

			if ( $user_from->amount < $amount ) {
				throw new \Exception( 'Sender has insufficient funds' ) ;
			}

			$user_to = UserModel::where( 'id' , $user_id_to )->
					where( 'banned' , '<' , true )->first( ) ;

			if ( empty( $user_to ) ) {
				throw new \Exception( 'Receiver not found' ) ;
			}

			$user_from->amount -= $amount ;
			$user_to->amount += $amount ;

			$user_from->save( ) ;
			$user_to->save( ) ;

			$trans = new self( ) ;
			$trans->user_id_from = $user_id_from ;
			$trans->user_id_to = $user_id_to ;
			$trans->amount = $amount ;

			$trans->save( ) ;
		} catch( \Exception $exception ) {
			DB::rollback( ) ;

			return $exception->getMessage( ) ;
		}

		DB::commit( ) ;

		return null ;
	}
}
