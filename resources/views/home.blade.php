<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" data-csrf="{{ csrf_token() }}">
	<head>
		<title>Task55</title>

		<meta charset="utf-8">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="/css/app.css" type="text/css">
		<link rel="stylesheet" href="/css/style.css" type="text/css">

		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="/js/app.js"></script>
		<script src="/js/script.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="authed session_id header">
				<form action="/logout" method="post" class="form-process form-logout">
					<label>
						<span>logout</span>
						<input type="submit" value="&rarr;">
					</label>
				</form>

				<form action="/user" method="post" class="ajax-update form-logout" data-timeout="6000">
					<div data-field="name" class="user-name"></div>
					<div data-field="amount" class="user-amount"></div>
				</form>

				<div class="both"></div>
			</div>

			<fieldset class="authed session_id body">
				<legend>
					<h2>Send payment</h2>
				</legend>
				<form action="/send" method="post" class="form-process form-send">
					<label>
						<span>receiver</span>
						<input name="user_id_to" required list="user_id_to">
					</label>
					<label>
						<span>amount</span>
						<input name="amount" type="number" required autocomplete="off">
					</label>
					<label>
						<span>send</span>
						<input type="submit" value="&rarr;">
					</label>
				</form>
				<form action="/users" method="post" data-timeout="20000" data-target="#user_id_to" class="ajax-update">
					<datalist id="user_id_to"></datalist>
				</form>
			</fieldset>

			<div class="left">
				<fieldset class="authed session_id table">
					<legend>
						<h2>Transactions</h2>
					</legend>
					<form action="/trans" method="post" data-timeout="7000" data-target=".trans" class="ajax-update">
						<span>Click on row to make similar payment</span>
						<table class="trans-table">
							<thead>
								<tr>
									<th>id</th>
									<th>sender</th>
									<th>receiver</th>
									<th>amount</th>
									<th>date\time</th>
								</tr>
							</thead>
							<tbody class="trans">
								<tr class="nod">
									<td class="trans-receiver-sender">
										<span data-field="receiver_id" class="trans-receiver_id"></span>
										<span data-field="sender_id" class="trans-sender_id"></span>
									</td>
									<td data-field="id"></td>
									<td data-field="sender_name"></td>
									<td data-field="receiver_name"></td>
									<td data-field="amount" class="trans-amount"></td>
									<td data-field="created_at"></td>
								</tr>
							</tbody>
						</table>
					</form>
				</fieldset>

				<fieldset class="authed session_id table">
					<legend>
						<h2>Users</h2>
					</legend>
					<form action="/users" method="post" data-timeout="10000" data-target=".users" class="ajax-update">
						<table class="users-table">
							<thead>
								<tr>
									<th>id</th>
									<th>name</th>
									<th>email</th>
									<th>admin</th>
									<th>banned</th>
								</tr>
							</thead>
							<tbody class="users">
								<tr class="nod">
									<td data-field="id"></td>
									<td data-field="name"></td>
									<td data-field="email"></td>
									<td data-field="admin"></td>
									<td data-field="banned"></td>
								</tr>
							</tbody>
						</table>
					</form>
				</fieldset>
				<div class="both"></div>
			</div>

			<fieldset class="unauthed session_id">
				<legend><h2>Login form</h2></legend>
				<form class="form-login form-process" action="/login" method="POST">
					<label>
						<span>e-mail:</span>
						<input name="email" type="email" required>
					</label>
					<label>
						<span>password:</span>
						<input name="passwd" type="password" required>
					</label>
					<label>
						<span>login</span>
						<input type="submit" value="&rarr;">
					</label>
				</form>
			</fieldset>
			<fieldset class="unauthed session_id">
				<legend><h2>Register form</h2></legend>
				<form class="form-register form-process" action="/register" method="POST">
					<label>
						<span>Full name:</span>
						<input name="name" pattern="\w[\w\s]*\w" required>
					</label>
					<label>
						<span>e-mail:</span>
						<input name="email" type="email" required>
					</label>
					<label>
						<span>password:</span>
						<input name="passwd" type="password" required>
					</label>
					<label>
						<span>register</span>
						<input type="submit" value="&rarr;">
					</label>
				</form>
			</fieldset>
		</div>
	</body>
</html>