<?php

use Illuminate\Database\Seeder;
use App\Trans as TransModel ;
use App\User as UserModel ;

class TransSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$user_count = 1e3 ; // UserModel::count( ) ;
	$user_id_from = $user_id_to = $amount = null ;

        for ( $i = 0 ; $i < $user_count ; $i ++ ) {
		while ( true ) {
			$user_id_from = rand( 1 , $user_count ) ;
			$user_id_to = rand( 1 , $user_count ) ;

			if ( $user_id_from != $user_id_to ) {
				break ;
			}
		}

		$amount = rand( 1 , 5e2 ) ;

		echo json_encode( [ $user_id_from , $user_id_to , $amount ] ) ;

		if ( TransModel::send( $user_id_from , $user_id_to , $amount ) ) {
			$i -- ;
		}
	}
    }
}
