<?php

use Illuminate\Database\Seeder;
use App\User as UserModel ;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	for ( $i = 0 ; $i < 0.3e3 ; $i ++ ) {
		try {
			UserModel::register(
				str_random( 10 ) ,
				str_random( 10 ) . '@' . str_random( 10 ) . '.' . str_random( 3 ) ,
				str_random( 10 ) ,
				! $i
			) ;
		} catch( \Exception $exception ) {
			echo $exception->getMessage( ) . PHP_EOL ;
		}
	}
        //
    }
}
