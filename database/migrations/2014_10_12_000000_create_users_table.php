<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
		$table->bigIncrements( 'id' ) ;
		$table->string( 'name' ) ;
		$table->string( 'email')->unique( ) ;
		$table->string( 'passwd' ) ;
		$table->bigInteger( 'amount' )->unsigned( )->default( 500 ) ;
		$table->string( 'session_id' )->nullable( true )->unique( ) ;
		$table->string( 'expires_at' )->nullable( true ) ;
		$table->boolean( 'admin' )->default( false ) ;
		$table->boolean( 'banned' )->default( false ) ;
		$table->timestamps( ) ;

		$table->index( [ 'session_id' , 'banned' ] ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
