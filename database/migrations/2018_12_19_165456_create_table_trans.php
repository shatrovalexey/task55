<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans', function (Blueprint $table) {
		$table->increments( 'id' ) ;
		$table->bigInteger( 'user_id_from' )->unsigned( ) ;
		$table->bigInteger( 'user_id_to' )->unsigned( ) ;
		$table->bigInteger( 'amount' )->unsigned( ) ;
		$table->timestamps( ) ;

		$table->index( 'user_id_from' ) ;
		$table->index( 'user_id_to' ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Trans');
    }
}
