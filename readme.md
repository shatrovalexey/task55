### THE TASK ###
* https://docs.google.com/document/d/1KRDjCcaj-Mbhdf2Y1EAI0068IuxK1Ih5Ohec296Dpko/edit
```
PW Application
Тестовое задание для создания небольшого приложения для осуществления переводов виртуальных денег (PW) между пользователями приложения. 
Задание приведено ниже на английском языке, рассматривайте его не как готовое ТЗ, а как первый материал, присланный заказчиком.

PW Application Overview
The application is for Parrot Wings (PW, “internal money”) transfer between system users.
The application will be very “polite” and will inform a user of any problems (i.e. login not successful, not enough PW to remit the transaction, etc.)
User registration 
Any person on Earth can register with the service for free, providing their Name (e.g. John Smith), valid email (e.g. jsmith@gmail.com) and password. 
When a new user registers, the System will verify, that the user has provided a unique (not previously registered in the system) email, and also provided human name and a password. These 3 fields are mandatory. Password is to be typed twice for justification. No email verification required.
On successful registration every User will be awarded with 500 (five hundred) PW starting balance.
Logging in 
Users login to the system using their email and password.
Users will be able to Log out.
No password recovery, change password, etc. functions required.
PW
The system will allow users to perform the following operations:
See their Name and current PW balance always on screen
Create a new transaction. To make a new transaction (PW payment) a user will
Choose the recipient by querying the  User list by name (autocomplete). 
When a recipient chosen, entering the PW transaction amount. The system will check that the transaction amount is not greater than the current user balance.
Committing the transaction. Once transaction succeeded, the recipient account will be credited (PW++) by the entered amount of PW, and the payee account debited (PW--) for the same amount of PW. The system shall display PW balance changes immediately to the user.
(Optional) Create a new transaction as a copy from a list of their existing transactions: create a handy UI for a user to browse their recent transactions, and select a transaction as a basis for a new transaction. Once old transaction selected, all its details (recipient, PW amount) will be copied to the new transaction.
Review a list (history) of their transactions. A list of transactions will show the most recent transactions on top of the list and display the following info for each transaction:
Date/Time of the transaction
Correspondent Name
Transaction amount, (Debit/Credit  for PW transferred)
Resulting balance
(Optional) Implement filtering and/or sorting of transaction list by date, correspondent name and amount. 
Administrative Panel
The system will allow administrative user to perform the following operations:
List/View/Edit users with BAN option; List should be filterable and sortable
List/View/Edit PW transactions; List should be filterable and sortable
Administrative user should be predefined.

Technical requirements
Use the following software:
Server side
DB - your suggests
PHP
PHP frameworks - your suggests. 
ORM
CMS
Administrative boilerplate
Business Entity generation
RESTfull or JSON WEB API is preferable not mandatory   
WEB HTML client
HTML5
Javascript or TypeScript
MVC, SPA - your suggests, SPA is is preferable not mandatory 
bootstrap

UI design requirements
Customer likes Material Design approach as well as native Mac OS look'n feel.
It is necessary to design and implement unique UI look'n feel for the front-end applications, for every UI elements, and (optionally) apply it to developed frontend application.
```

### API ###
```
POST /csrf - get CSRF token
POST /login, email, passwd, csrf - login
POST /logout, session_id, csrf - logout
POST /user, session_id, csrf - get user data
POST /users, session_id, csrf - get all users data
POST /register, name, email, passwd, csrf - registration
POST /send, session_id, user_id_to, amount - send amount to user_id_to
POST /trans, session_id, csrf - get current user's transactions
```

### INSTALLATION ###
```
git clone https://bitbucket.org/shatrovalexey/task55.git
cd task55
composer install
cp -p .env.example .env
# edit .env, MySQL dialect is using
./artisan key:generate
./artisan migrate
./artisan db:seed --class=UsersSeeder # if you need to seed
#./artisan serve # или cd publish ; php -S ${IP}:${PORT}
```

### AUTHOR ###
Shatrov Aleksej <mail@ashatrov.ru>